﻿using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.BL.Exceptions
{
    public class NotDistinctVehicleException : Exception
    {
        public string ProblemId { get; set; }
        public NotDistinctVehicleException()
        {
        }

        public NotDistinctVehicleException(string message)
            : base(message)
        {
        }

        public NotDistinctVehicleException(string message, string problemId)
            : base(message)
        {
            this.ProblemId = problemId;
        }

        public NotDistinctVehicleException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }

    class InsufficientBalanceException : Exception
    {
        public Vehicle ProblemVehicle { get; set; }
        public InsufficientBalanceException()
        {
        }

        public InsufficientBalanceException(string message)
            : base(message)
        {
        }

        public InsufficientBalanceException(string message, Vehicle problemVehicle)
            : base(message)
        {
            this.ProblemVehicle = problemVehicle;
        }

        public InsufficientBalanceException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
