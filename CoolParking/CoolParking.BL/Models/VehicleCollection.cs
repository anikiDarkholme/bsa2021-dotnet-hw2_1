﻿using CoolParking.BL.Exceptions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace CoolParking.BL.Models
{
    public class VehicleCollection : IEnumerable<Vehicle>, IEnumerable, ICollection<Vehicle>
    {
        List<Vehicle> vehicles = new List<Vehicle>();

        public int Count => this.vehicles.Count();

        public bool IsReadOnly => false;

        public ReadOnlyCollection<Vehicle> AsReadOnly() => vehicles.AsReadOnly();

        /// /// <exception cref="CoolParking.BL.Exceptions.NotDistinctVehicleException">Thrown when the vehicle 
        /// given already exists in the collection.</exception>
        public void Add(Vehicle item)
        {
                this.vehicles.Add(item);
        }

        public void Clear()
        {
            vehicles.Clear();
        }

        public bool Contains(Vehicle item)
        {
            return this.vehicles.Contains(item);
        }

        public void CopyTo(Vehicle[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public bool Remove(Vehicle item)
        {
            if (this.vehicles.Remove(item))
                return true;

            else
                return false;

        }

        /// <exception cref="System.Collections.Generic.KeyNotFoundException">Thrown when the vehicle 
        /// with a given Id is not in the parking.</exception>
        /// <exception cref="CoolParking.BL.Exceptions.InsufficientBalanceException">Thrown when the vehicle's
        /// balance is not sufficient.</exception>
        public bool Remove(string vehicleId)
        {
            Vehicle FoundVehicle;
            try
            {
                FoundVehicle = GetVehicle(vehicleId);

                if (FoundVehicle.Balance < 0)
                    throw new InsufficientBalanceException("Insuffiecient balance");

                try
                {
                    while (this.Contains(FoundVehicle))
                    {
                        Remove(FoundVehicle);

                        FoundVehicle = GetVehicle(vehicleId);
                    }
                }
                catch { }
                return true;
            }
            catch
            {
                throw;
            }
        }

        /// <exception cref="System.Collections.Generic.KeyNotFoundException">Thrown when the vehicle 
        /// with a given Id is not in the parking.</exception>
        public Vehicle GetVehicle(string vehicleId)
        {
            var SearchedVehicle =
            this.vehicles.Where(n => n.Id == vehicleId).ToList();

            if (SearchedVehicle.Count() < 1)
                throw new KeyNotFoundException("List doesn't contain the vehicle required");

            return SearchedVehicle.First();
        }

        #region enS
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        IEnumerator<Vehicle> GetEnumerator()
        {
            foreach (Vehicle foo in this.vehicles)
            {
                yield return foo;
            }
        }

        IEnumerator<Vehicle> IEnumerable<Vehicle>.GetEnumerator()
        {
            foreach (Vehicle foo in this.vehicles)
            {
                yield return foo;
            }
        }
        #endregion
    }

}