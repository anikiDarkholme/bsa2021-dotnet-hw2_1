﻿using System.Collections.Generic;


namespace CoolParking.BL.Models
{
    static class Settings
    {
        public static decimal InitParkingBalance { get; set; } = 0m;

        public static int ParkingCapacity { get; set; } = 10;

        /// <summary>
        /// Given in seconds
        /// </summary>
        public static int ParkingPaymentPeriod { get; set; } = 5;

        /// <summary>
        /// Given in seconds
        /// </summary>
        public static int ParkingLoggingPeriod { get; set; } = 60;

        public static Dictionary<VehicleType, decimal> VehicleTypePaymentRates { get; set; }
            = new Dictionary<VehicleType, decimal>()
            {
            { VehicleType.PassengerCar, 2m },
            { VehicleType.Truck , 5m },
            { VehicleType.Bus , 3.5m },
            { VehicleType.Motorcycle , 1m },
            };

        public static decimal PenaltyRate { get; set; } = 2.5m;
    }

}