﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using Fare;
using System;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text.Json;
using System.ComponentModel.DataAnnotations;
using CoolParking.BL.Services;
using CoolParking.BL.Exceptions;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        [ValidVehicle]
        public string Id { get; set; }

        [EnumDataType(typeof(VehicleType))]
        public VehicleType VehicleType { get; set; }

        [ValidSum]
        public decimal Balance { get; set; }

        public Vehicle()
        { }

        public Vehicle(string Id, VehicleType vehicleType, decimal balance)
        {
            if (!ValidateId(Id))
            {
                throw new ArgumentException("Given Id is not in a correct format");
            }

            if (balance < 0)
            {
                throw new ArgumentException("Not a valid balance passed");
            }

            this.Id = Id;

            this.VehicleType = vehicleType;

            this.Balance = balance;
        }

        public static bool ValidateId(string Id)
        {
            Regex IdValidator = new Regex(@"^[A-Z]{2}-\d{4}-[A-Z]{2}$");

            return IdValidator.IsMatch(Id) ? true : false;
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {

            string IdSeed = @"^[A-Z]{2}-\d{4}-[A-Z]{2}$";

            Xeger IdGenerator = new Xeger(IdSeed, new Random());

            return IdGenerator.Generate();
        }

        public override string ToString()
        {
                string TextRepresentation = string.Empty;

                TextRepresentation = $"{this.Id} {this.VehicleType} {this.Balance}";

                return TextRepresentation;
        }
    }

    public class ValidVehicleAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value,
            ValidationContext validationContext)
        {
            if (value == null)
            {
                return new ValidationResult("Null passed as an Id");
            }

            if (Vehicle.ValidateId((string)value))
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult("Id is invalid");
            }
        }
    }

}
