﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.BL.Models
{
    public class TopUpModel
    {
        [ValidVehicle]
        public string Id { get; set; }

        [ValidSum]
        public decimal Sum { get; set; }
    }

    public class ValidSumAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value,
            ValidationContext validationContext)
        {
            if ((decimal)value <= 0)
            {
                return new ValidationResult("Invalid sum passed");
            }
            return ValidationResult.Success;
        }
    }
}
