﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using CoolParking.BL.Exceptions;
using CoolParking.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace CoolParking.BL.Models
{

    class Parking
    {
        private Parking() { }

        private static Parking _instance;

        public VehicleCollection Vehicles { get; set; } = new VehicleCollection();

        public decimal Balance { get; set; } = Settings.InitParkingBalance;

        public static Parking GetInstance()
        {
            if (_instance == null)
                _instance = new Parking();

            return _instance;
        }

    }
}