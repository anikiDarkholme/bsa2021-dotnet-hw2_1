﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL;
using System.Collections.ObjectModel;
using System;
using CoolParking.BL.Exceptions;
using System.Collections.Generic;
using System.Timers;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private Parking _parking;

        private ILogService _logSevice;

        private readonly ITimerService _logTimer;

        private readonly ITimerService _withdrawTimer;

        private List<TransactionInfo> _transactionHistory = new List<TransactionInfo>();

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            this._parking = Parking.GetInstance();

            this._parking.Balance = Settings.InitParkingBalance;

            withdrawTimer.Interval = Settings.ParkingPaymentPeriod;
            withdrawTimer.Elapsed += WithdrawVehicles;
            this._withdrawTimer = withdrawTimer;

            logTimer.Interval = Settings.ParkingLoggingPeriod;
            logTimer.Elapsed += LogTransactions;
            this._logSevice = logService;
            this._logTimer = logTimer;
        }

        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public int GetCapacity()
        {
            return Settings.ParkingCapacity;
        }

        public int GetFreePlaces()
        {
            return Settings.ParkingCapacity - _parking.Vehicles.Count;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return this._parking.Vehicles?.AsReadOnly();
        }

        /// <exception cref="System.ArgumentException">Thrown when the vehicle model given 
        /// is somehow invalid.</exception>
        /// <exception cref="System.InvalidOperationException">Thrown when trying to add
        /// a vehicle to the full parking
        public void AddVehicle(Vehicle vehicle)
        {
            if (vehicle == null)
                throw new ArgumentNullException("Trying to pass a null reference as a vehicle to the parking");

            if (this.GetFreePlaces() < 1)
                throw new InvalidOperationException("Trying to add a vehicle to the full parking");
            try
            {
                this._parking.Vehicles.Add(vehicle);
            }
            catch { }
        }

        /// <exception cref="System.ArgumentException">Thrown when the id given 
        /// is somehow invalid.</exception>
        ///<exception cref="System.InvalidOperationException">Thrown when trying to remove
        /// a vehicle with negative balance
        /// <exception cref="ArgumentNullException">Thrown when trying to pass null
        /// as an id</exception>
        public void RemoveVehicle(string vehicleId)
        {
            if (vehicleId == null)
                throw new ArgumentNullException("Trying to pass a null reference as a vehicleId to the parking");

            try
            {
                this._parking.Vehicles.Remove(vehicleId);

            }
            catch (InsufficientBalanceException ex)
            {
                throw new InvalidOperationException("Insufficient funds to remove from the parking", ex);
            }
            catch (KeyNotFoundException ex)
            {
                throw new ArgumentException("Wrong vehicle Id", ex);
            }

        }

        /// <exception cref="System.ArgumentException">Thrown when the id given 
        /// is somehow invalid.</exception>
        /// <exception cref="NotDistinctVehicleException">Thrown when trying to top up
        /// a non-distinct vehicle
        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum < 0) throw new ArgumentException("Invalid sum passed as to the transaction");
            Vehicle ToppedUpVehicle;
            try
            {
                ToppedUpVehicle = this._parking.Vehicles.GetVehicle(vehicleId);
                ToppedUpVehicle.Balance += sum;
            }
            catch (NotDistinctVehicleException)
            {
                throw;
            }
            catch (KeyNotFoundException ex)
            {
                throw new ArgumentException("Wrong vehicle Id", ex);
            }
        }

        public void WithdrawVehicles(object sender, ElapsedEventArgs e)
        {
            foreach (var vehicle in this._parking.Vehicles)
                WithdrawVehicle(vehicle);

            void WithdrawVehicle(Vehicle vehicle)
            {
                decimal CurrentRate = Settings.VehicleTypePaymentRates[vehicle.VehicleType];
                decimal CurrentPenaltyRate = Settings.PenaltyRate;
                decimal Payment;

                if (vehicle.Balance > CurrentRate)
                { Payment = CurrentRate; }
                else
                {
                    Payment =
                              Math.Max(0, vehicle.Balance) + (CurrentRate - Math.Max(0, vehicle.Balance)) * CurrentPenaltyRate;
                }

                vehicle.Balance -= Payment;
                this._parking.Balance += Payment;

                this._transactionHistory.Add(
                    new TransactionInfo()
                    {
                        Sum = Payment,
                        TransactionTime = DateTime.Now,
                        VehicleId = vehicle.Id
                    });

            }
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return this._transactionHistory.ToArray();
        }

        public Vehicle GetVehicle(string vehicleId)
        {
            if (vehicleId == null)
                throw new ArgumentNullException("Trying to pass a null reference as a vehicleId to the parking");

            try
            {
                var returnedVehicle = this._parking.Vehicles.GetVehicle(vehicleId);

                return returnedVehicle;
            }
            catch (KeyNotFoundException ex)
            {
                throw new ArgumentException("Wrong vehicle Id", ex);
            }
        }

        private void LogTransactions(object sender, ElapsedEventArgs e)
        {
            foreach (var Transaction in this._transactionHistory)
            {
                _logSevice.Write(Transaction.ToString());
            }

            this._transactionHistory.Clear();

            _logSevice.Write(" ");
        }

        public string ReadFromLog()
        {
            try
            {
                string Logs = _logSevice.Read();

                return Logs;
            }
            catch (InvalidOperationException ex)
            {
                throw;
            }

        }

        public void Dispose()
        {
            this._parking.Vehicles.Clear();

            this._parking = null;

            this._logTimer.Dispose();

            this._withdrawTimer.Dispose();
        }


    }

}