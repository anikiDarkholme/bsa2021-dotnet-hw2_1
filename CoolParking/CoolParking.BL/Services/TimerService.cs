﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using System;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private Timer _timer;

        public event ElapsedEventHandler Elapsed;

        public double Interval { get; set; }

        public void Start()
        {
            _timer = new Timer(TimeSpan.FromSeconds(Interval).TotalMilliseconds);
            _timer.Elapsed += Elapsed;
            _timer.AutoReset = true;
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Elapsed -= Elapsed;
            _timer.Stop();
        }

        public void Dispose()
        {
            _timer.Dispose();
        }
    }

}