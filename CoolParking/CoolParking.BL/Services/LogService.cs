﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using CoolParking.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public LogService(string logPath)
        {
            this.LogPath = logPath;
        }

        public string LogPath { get; } = "Transactions.log";

        public void Write(string logInfo)
        {
            StreamWriter writer = null;

            try
            {
                writer = new StreamWriter($"{LogPath}", append: true);

                writer.WriteLine(logInfo);
                writer.Flush();

            }
            catch
            {
                throw;
            }
            finally
            {
                writer.Dispose();
            }
        }

        /// <exception cref="InvalidOperationException">Thrown when the logs file
        /// was not found.</exception>
        public string Read()
        {
            StreamReader reader = null;
            try
            {
                reader = new StreamReader($"{LogPath}");

                string ReadLog = reader.ReadToEnd();

                return ReadLog;
            }
            catch (FileNotFoundException ex)
            {
                throw new InvalidOperationException("Specified LogPath doesn't exist.", ex);
            }
            finally
            {
                reader.Dispose();
            }
        }
    }
}