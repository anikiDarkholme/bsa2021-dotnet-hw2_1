﻿namespace CoolParking.BL.Interfaces
{
    public interface ILogService
    {
        string LogPath { get; }
        void Write(string logInfo);
        /// <exception cref="InvalidOperationException">Thrown when the logs file
        /// was not found.</exception>
        string Read();
    }
}