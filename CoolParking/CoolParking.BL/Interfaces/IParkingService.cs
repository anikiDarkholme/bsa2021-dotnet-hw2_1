﻿using CoolParking.BL.Models;
using System;
using System.Collections.ObjectModel;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
namespace CoolParking.BL.Interfaces
{
    public interface IParkingService : IDisposable
    {
        decimal GetBalance();
        int GetCapacity();
        int GetFreePlaces();
        ReadOnlyCollection<Vehicle> GetVehicles();
        /// <exception cref="System.ArgumentException">Thrown when the vehicle model given 
        /// is somehow invalid.</exception>
        /// <exception cref="System.InvalidOperationException">Thrown when trying to add
        /// a vehicle to the full parking
        void AddVehicle(Vehicle vehicle);
        /// <exception cref="System.ArgumentException">Thrown when the id given 
        /// is somehow invalid.</exception>
        ///<exception cref="System.InvalidOperationException">Thrown when trying to remove
        /// a vehicle with negative balance
        /// <exception cref="NotDistinctVehicleException">Thrown when trying to remove
        /// a non-distinct vehicle
        void RemoveVehicle(string vehicleId);
        /// <exception cref="System.ArgumentException">Thrown when the id given 
        /// is somehow invalid.</exception>
        /// <exception cref="NotDistinctVehicleException">Thrown when trying to top up
        /// a non-distinct vehicle
        void TopUpVehicle(string vehicleId, decimal sum);
        TransactionInfo[] GetLastParkingTransactions();
        string ReadFromLog();
    }
}
