﻿using System;

namespace ParkingInterface
{
    class Program
    {
     
        static void Main(string[] args)
        {
            Console.CursorVisible = false;
            MainLoop();
        }

        private static void MainLoop()
        {
            ConsoleKeyInfo ReadButton;
            while (true)
            {
                Menu.Draw();
                ReadButton = Console.ReadKey(intercept: true);
                Menu.ChangeSelectedMenu(ReadButton.Key);
            }
        }
    }
}
