﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using DrawRectangle;
using System.Linq;

namespace ParkingInterface
{
    static class Menu
    {
        static Menu()
        {
            int Xposition = 1, Yposition = 1;

            #region LeftMenuConfig
            LeftColumn.Add(
                new ConsoleRectangle(
                    25, 3, new Point() { X = Xposition, Y = Yposition },
                    ConsoleColor.White,
                    new List<string>()
                    { "Current Balance" },
                    1
                ));
            LeftColumn.Add(
               new ConsoleRectangle(
                   25, 3, new Point() { X = Xposition, Y = Yposition += 5 },
                   ConsoleColor.White,
                   new List<string>()
                   { "Last Transactions sum" },
                   1
               ));
            LeftColumn.Add(
               new ConsoleRectangle(
                   25, 3, new Point() { X = Xposition, Y = Yposition += 5 },
                   ConsoleColor.White,
                   new List<string>()
                   { "Free Spaces" },
                   1
               ));
            LeftColumn.Add(
               new ConsoleRectangle(
                   25, 3, new Point() { X = Xposition, Y = Yposition += 5 },
                   ConsoleColor.White,
                   new List<string>()
                   { "Current Transactions" },
                   1
               ));
            LeftColumn.Add(
               new ConsoleRectangle(
                   25, 3, new Point() { X = Xposition, Y = Yposition += 5 },
                   ConsoleColor.White,
                   new List<string>()
                   { "Logged Transactions" },
                   1
               ));
            LeftColumn.Add(
             new ConsoleRectangle(
                 25, 3, new Point() { X = Xposition, Y = Yposition += 5 },
                 ConsoleColor.White,
                 new List<string>()
                 { "Parked Vehicles" },
                 1
             ));
            #endregion

            Yposition = 1;
            Xposition += 45;

            #region RightMenuConfig
            RightColumn.Add(
               new ConsoleRectangle(
                   20, 3, new Point() { X = Xposition, Y = Yposition },
                   ConsoleColor.White,
                   new List<string>()
                   { "Add a vehicle" },
                   1
               ));
            RightColumn.Add(
               new ConsoleRectangle(
                   20, 3, new Point() { X = Xposition, Y = Yposition += 5 },
                   ConsoleColor.White,
                   new List<string>()
                   { "Remove a vehicle" },
                   1
               ));
            RightColumn.Add(
               new ConsoleRectangle(
                   20, 3, new Point() { X = Xposition, Y = Yposition += 5 },
                   ConsoleColor.White,
                   new List<string>()
                   { "Top up a vehicle" },
                   1
               ));
            #endregion

            #region InfoBoxConfig

            InfoBox = new ConsoleRectangle(
                   35, 7, new Point() { X = 45, Y = 30 },
                   ConsoleColor.Green,
                   new List<string>()
                   { "Use arrows to navigate" ,
                       " \n " ,
                       "Enter to select the menu" ,
                         " \n " ,
                       "Esc to stop the execution",
                       " \n " ,
                       "And yeah, this whole ",
                       " \n " ,
                       "interface is trashy)) " ,
                   },
                   1
               );
            #endregion
        }

        public static ConsoleRectangle InfoBox;

        public static List<ConsoleRectangle> LeftColumn = new List<ConsoleRectangle>();

        public static List<ConsoleRectangle> RightColumn = new List<ConsoleRectangle>();

        public static List<ConsoleRectangle> SelectedColumn = LeftColumn;

        public static int SelectedRectangleNumber = 0;
        public static void Draw()
        {
            List<ConsoleRectangle> NotSelectedColumn;
            if (SelectedColumn == LeftColumn)
                NotSelectedColumn = RightColumn;
            else
                NotSelectedColumn = LeftColumn;

            for (int i = 0; i < SelectedColumn.Count; i++)
            {
                if (i == SelectedRectangleNumber)
                {
                    SelectedColumn[i].BorderColor = ConsoleColor.Blue;
                    SelectedColumn[i].TitleColor = ConsoleColor.Blue;

                    SelectedColumn[i].Draw();
                    SelectedColumn[i].AlertTitle();

                    SelectedColumn[i].BorderColor = ConsoleColor.White;
                    SelectedColumn[i].TitleColor = ConsoleColor.White;
                }

                else

                SelectedColumn[i].Draw();
                SelectedColumn[i].AlertTitle();


            }
            for (int i = 0; i < NotSelectedColumn.Count; i++)
            {
                NotSelectedColumn[i].Draw();
                NotSelectedColumn[i].AlertTitle();
            }

            InfoBox.Draw();
            InfoBox.AlertTitle();

            Console.SetCursorPosition(0, 0);
        }

        public static void ChangeSelectedMenu(ConsoleKey pressedKey)
        {
            switch (pressedKey)
            {
                case ConsoleKey.LeftArrow:
                    SelectedColumn = LeftColumn;
                    break;

                case ConsoleKey.RightArrow:
                    SelectedColumn = RightColumn;
                    if (SelectedRectangleNumber > RightColumn.Count - 1)
                        SelectedRectangleNumber = RightColumn.Count - 1;
                    break;

                case ConsoleKey.UpArrow:
                    if (SelectedRectangleNumber > 0) SelectedRectangleNumber--;
                    break;

                case ConsoleKey.DownArrow:
                    if (SelectedRectangleNumber < SelectedColumn.Count - 1) SelectedRectangleNumber++;
                    break;

                case ConsoleKey.Enter:
                    DoSomething();
                    Draw();
                    break;


            }

        }

        private async static void DoSomething()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(@"https://localhost:5001/api/");
            InfoBox.FileText = "";
            InfoBox.Draw();

            static async Task<string> RetrieveBodyByUrl(HttpClient client, string localUrl)
            {
                var responseObject = (await client.GetAsync(client.BaseAddress + localUrl));

                if (responseObject.IsSuccessStatusCode)
                {
                    return await responseObject.Content.ReadAsStringAsync();
                }
                else { return "Connection failed"; }
            }

            string RequestBody = string.Empty;

            if (SelectedColumn == LeftColumn)
                switch (SelectedRectangleNumber)
                {
                    case 0:
                        RequestBody = await RetrieveBodyByUrl(client, "parking/balance");
                        ConfigInfoBox(RequestBody + " units", 3);
                        break;

                    case 1:
                        RequestBody = await RetrieveBodyByUrl(client, "transactions/last");
                        var transactionHistory = JsonConvert.DeserializeObject<TransactionInfo[]>(RequestBody);

                        decimal sum = 0m;
                        foreach (var item in transactionHistory)
                        {
                            sum += item.Sum;
                        }
                        ConfigInfoBox(sum.ToString(), 3);
                        break;

                    case 2:
                        ConfigInfoBox(await RetrieveBodyByUrl(client, "parking/freePlaces"), 3);
                        break;

                    case 3:
                        string trans = await RetrieveBodyByUrl(client, "transactions/last");
                        var recenttransactionHistory = JsonConvert.DeserializeObject<TransactionInfo[]>(trans);
                        string LogText = string.Empty;

                        foreach (var Transaction in recenttransactionHistory)
                        {
                            LogText += Transaction.ToString() + "\n";
                        }
                        ConfigInfoBox(LogText, recenttransactionHistory.Length + 2);
                        break;

                    case 4:
                        string transactionText = await RetrieveBodyByUrl(client, "transactions/all");
                        var multTransactionsText = new List<string>(transactionText.Split(new char[]{'\n','\r'}, Console.BufferHeight-50, StringSplitOptions.RemoveEmptyEntries));
                        multTransactionsText = multTransactionsText
                            .Where(n => n.Length > 1)
                            .SkipLast(1)
                            .ToList();

                        string TextRepresentation = string.Empty;
                        foreach (var item in multTransactionsText)
                        {
                            TextRepresentation += item + "\n";
                        }

                        ConfigInfoBox(TextRepresentation, Console.BufferHeight - 48);
                        break;

                    case 5:
                        string vehs = await RetrieveBodyByUrl(client, "vehicles");
                        var vehicles = JsonConvert.DeserializeObject<Vehicle[]>(vehs);
                        string VehiclesText = string.Empty;

                        foreach (var item in vehicles)
                        {
                            VehiclesText += item.ToString() + "\n";
                        }
                        ConfigInfoBox(VehiclesText, vehicles.Length);
                        break;
                }
            else
                switch (SelectedRectangleNumber)
                {
                    case 0:
                        try
                        {
                            InfoBox.Draw();
                            Console.SetCursorPosition(InfoBox.Location.X, InfoBox.FileTextY);
                            Console.Write("Id: ");
                            string Id = Console.ReadLine();
                            Console.SetCursorPosition(InfoBox.Location.X, InfoBox.FileTextY += 2);
                            Console.Write("VehicleType: [0|1|2|3]");
                            int type = Convert.ToInt32(Console.ReadLine());
                            Console.SetCursorPosition(InfoBox.Location.X, InfoBox.FileTextY += 2);
                            Console.Write("Balance: ");
                            decimal balance = Convert.ToDecimal(Console.ReadLine());


                            using (var request = new HttpRequestMessage(HttpMethod.Post, client.BaseAddress + "vehicles"))
                            {
                                var json = JsonConvert.SerializeObject(new { ID = Id, vehicleType = type, Balance = balance });
                                using (var stringContent = new StringContent(json, Encoding.UTF8, "application/json"))
                                {
                                    request.Content = stringContent;

                                    using (var response = await client
                                        .SendAsync(request, HttpCompletionOption.ResponseHeadersRead)
                                        .ConfigureAwait(false))
                                    {
                                        response.EnsureSuccessStatusCode();
                                    }
                                }
                            }

                            ConfigInfoBox("Added", 5);
                        }
                        catch { }
                        break;
                    case 1:
                        try
                        {

                            InfoBox.Draw();
                            Console.SetCursorPosition(InfoBox.Location.X, InfoBox.FileTextY);
                            Console.Write("Id: ");
                            string RemovedId = Console.ReadLine();

                            var result = await client.DeleteAsync(client.BaseAddress + $"vehicles/{RemovedId}");
                            ConfigInfoBox(result.Content.ReadAsStringAsync().Result, 5);
                        }
                        catch { }
                        break;
                    case 2:
                        try
                        {
                            InfoBox.Draw();
                            Console.SetCursorPosition(InfoBox.Location.X, InfoBox.FileTextY);
                            Console.Write("Id: ");
                            string Id = Console.ReadLine();
                            Console.SetCursorPosition(InfoBox.Location.X, InfoBox.FileTextY += 2);
                            Console.Write("Balance: ");
                            decimal balance = Convert.ToDecimal(Console.ReadLine());

                            using (var request = new HttpRequestMessage(HttpMethod.Put, client.BaseAddress + "transactions/topUpVehicle"))
                            {
                                var json = JsonConvert.SerializeObject(new { ID = Id, Balance = balance });
                                using (var stringContent = new StringContent(json, Encoding.UTF8, "application/json"))
                                {
                                    request.Content = stringContent;

                                    using (var response = await client
                                        .SendAsync(request, HttpCompletionOption.ResponseHeadersRead)
                                        .ConfigureAwait(false))
                                    {
                                        response.EnsureSuccessStatusCode();
                                    }
                                }
                            }

                            ConfigInfoBox("Put", 5);
                        }
                        catch { }
                        break;
                }
        }

        private static void ConfigInfoBox(string text, int lines)
        {
            InfoBox.FileText = text;
            InfoBox.Height = lines+2;
        }
    }
}
