﻿using CoolParking.BL.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Models;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/transactions")]
    public class TransactionsController : ControllerBase
    {
        public ParkingService ParkingService { get; }

        public TransactionsController(ParkingService parkingService)
        {
            ParkingService = parkingService;
        }

        [HttpGet("last")]
        public IActionResult GetLastLogInfo()
        {
            var returnedTransactions = ParkingService.GetLastParkingTransactions();

            return Ok(returnedTransactions.ToArray());
        }

        [HttpGet("all")]
        public IActionResult GetAllLoggedLogInfo()
        {
            string returnedLog = null;
            try
            {
                returnedLog = ParkingService.ReadFromLog();

                return Ok(returnedLog);
            }
            catch ( InvalidOperationException ex )
            {
                return NotFound(ex.Message);
            }
            catch ( Exception ex )
            {
                return NotFound(ex.Message);
            }
        }   

        [HttpPut("topUpVehicle")]
        public IActionResult TopUpVehicle([FromBody] TopUpModel topUpObject)
        {
            try
            {
                var foundVehicle = ParkingService.GetVehicle(topUpObject.Id);

                foundVehicle.Balance += topUpObject.Sum;

                return Ok(foundVehicle);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }

    }
    }
}

