﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Exceptions;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/vehicles")]
    public class VehiclesController : ControllerBase
    {
        public ParkingService ParkingService { get; }

        public VehiclesController(ParkingService parkingService)
        {
            ParkingService = parkingService;
        }

        [HttpGet]
        [Route("")]
        public IActionResult GetVehicles()
        {
            var returnedVehicles = ParkingService.GetVehicles();

            return Ok(returnedVehicles);
        }

        [HttpPost]
        [Route("")]
        public IActionResult PostVehicle([FromBody] Vehicle vehicle)
        {
            try
            {
                ParkingService.AddVehicle(vehicle);

                return CreatedAtAction(nameof(GetVehicleById), new { Id = vehicle.Id }, vehicle);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult GetVehicleById([ValidVehicle] string id)
        {
            Vehicle returnedVehicle;

            try
            {
                returnedVehicle = ParkingService.GetVehicle(id);
            }
            catch (InvalidOperationException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ArgumentException ex)
            {
                return NotFound(ex.Message);
            }
            return Ok(
                returnedVehicle
                 );
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult DeleteVehicleById([ValidVehicle] string id)
        {
            try
            {
                ParkingService.RemoveVehicle(id);

                return NoContent();
            }
            catch (InvalidOperationException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ArgumentException ex)
            {
                return NotFound(ex.Message);
            }
        }
    }
}
