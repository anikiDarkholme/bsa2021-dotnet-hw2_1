﻿using CoolParking.BL.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/parking")]
    public class ParkingController : ControllerBase
    {
        public ParkingService ParkingService { get; }

        public ParkingController(ParkingService parkingService)
        {
            ParkingService = parkingService;
        }

        [HttpGet]
        [Route("balance")]
        public IActionResult GetBalance()
        {

            decimal returnedBalance = ParkingService.GetBalance();

            return Ok(returnedBalance);

        }

        [HttpGet]
        [Route("capacity")]
        public IActionResult GetCapacity()
        {

            int returnedCapacity = ParkingService.GetCapacity();

            return Ok(returnedCapacity);

        }

        [HttpGet]
        [Route("freePlaces")]
        public IActionResult GetFreePlaces()
        {
            int returnedFreePlaces = ParkingService.GetFreePlaces();

            return Ok(returnedFreePlaces);
        }
    }
}
